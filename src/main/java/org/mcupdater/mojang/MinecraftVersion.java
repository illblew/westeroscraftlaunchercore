package org.mcupdater.mojang;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.mcupdater.util.MCUpdater;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/*
 * Implementation of version.json format used by Minecraft's launcher
 */
public class MinecraftVersion {
	private String id;
	private String time;
	private String releaseTime;
	private String type;
	private String minecraftArguments;
	private int minimumLauncherVersion;
	private List<Library> libraries;
	private String mainClass;
	private String incompatibilityReason;
    private String assets;
	private List<Rule> rules;
	
	public String getId(){ return id; }
	public String getTime(){ return time; }
	public String getReleaseTime(){ return releaseTime; }
	public String getType(){ return type; }
	public String getMinecraftArguments(){ return minecraftArguments; }
	public int getMinimumLauncherVersion(){ return minimumLauncherVersion; }
	public List<Library> getLibraries(){ return libraries; }
	public String getMainClass(){ return mainClass; }
	public String getIncompatibilityReason(){ return incompatibilityReason; }
    public String getAssets() { return this.assets; }
	public List<Rule> getRules(){ return rules; }
	
	public static MinecraftVersion loadVersion(String version) {
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());
		builder.enableComplexMapKeySerialization();
		Gson gson = builder.create();
		File versionCache = new File(MCUpdater.getInstance().getArchiveFolder().toFile(), "version-" + version + ".json");
		
		URLConnection conn;
        InputStream in = null;
        MinecraftVersion v = null;
		try {
		    MCUpdater.apiLogger.info("Fetching MC version info for " + version);
		    URL url = new URL("https://s3.amazonaws.com/Minecraft.Download/versions/" + version + "/" + version + ".json");
			conn = url.openConnection();
			in = conn.getInputStream();
			v = gson.fromJson(new InputStreamReader(in),MinecraftVersion.class);
			if (v != null) {     // Loaded - update cached copy
			    MCUpdater.apiLogger.info("Updating version cache for " + version);
			    String enc = gson.toJson(v);
			    try {
			        FileWriter fw = new FileWriter(versionCache);
			        fw.write(enc);
			        fw.close();
			    } catch (IOException iox) {
			        MCUpdater.apiLogger.severe("Error writing version cache " + versionCache.getPath());
			    }
			}
			return v;
		} catch (MalformedURLException e) {
		    MCUpdater.apiLogger.severe("Bad URL for MC version");
		} catch (IOException e) {
            MCUpdater.apiLogger.severe("Error reading version URL");
		} finally {
		    if (in != null) {
		        try { in.close(); } catch (IOException iox) {}
		        in = null;
		    }
		}
		// If not loaded, try cache
		if ((v == null) && versionCache.exists()) {
		    MCUpdater.apiLogger.info("Loading version info from + " + versionCache.getPath());
		    try {
		        in = new FileInputStream(versionCache);
		        v = gson.fromJson(new InputStreamReader(in), MinecraftVersion.class);
		    } catch (IOException iox) {
	            MCUpdater.apiLogger.severe("Error reading version cache " + versionCache.getPath());
	        } finally {
	            if (in != null) {
	                try { in.close(); } catch (IOException iox) {}
	                in = null;
	            }
	        }
		}
		return v;
	}
}
