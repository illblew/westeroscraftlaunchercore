package org.mcupdater;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.mcupdater.MCUConsole.LogLine;

public class ConsoleHandler extends Handler {
	private MCUConsole console;
	private final SimpleDateFormat sdFormat = new SimpleDateFormat("[HH:mm:ss.SSS] "); 

	public ConsoleHandler(MCUConsole console) {
		this.console = console;
	}
	
	@Override
	public void publish(final LogRecord record) {
		if (this.isLoggable(record)){
            String msg = record.getMessage();
            if (MainShell.isFilteredMsg(msg)) {
                return;
            }
			final Calendar recordDate = Calendar.getInstance();
			recordDate.setTimeInMillis(record.getMillis());
			Throwable thrown = record.getThrown();
            String fmtmsg = sdFormat.format(recordDate.getTime()) + msg + (thrown != null ? " (stacktrace in " + record.getLoggerName() + " log)" : "");
            MainShell.getInstance().consoleWrite(fmtmsg);
		}
	}	

	@Override
	public void flush() {}

	@Override
	public void close() throws SecurityException {}

}
