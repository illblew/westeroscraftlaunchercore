package org.mcupdater;

import java.io.IOException;
import java.util.Properties;

public class Version {
	public static final int MAJOR_VERSION;
	public static final int MINOR_VERSION;
	public static final String BUILD_BRANCH;
	public static final String BUILD_LABEL;
	public static final int API_MAJOR = 3;
	public static final int API_MINOR = 0;
	static {
		Properties prop = new Properties();
		try {
			prop.load(Version.class.getResourceAsStream("/version.properties"));
		} catch (IOException e) {
		}
		String vers = prop.getProperty("version");
		String[] v = vers.split("[.]");
		MAJOR_VERSION = Integer.valueOf(v[0]);
		MINOR_VERSION = Integer.valueOf(v[1]);
		BUILD_LABEL = prop.getProperty("build_version","0");
		BUILD_BRANCH = prop.getProperty("git_branch","unknown");
	}
	
	public static final String API_VERSION = API_MAJOR + "." + API_MINOR;
	public static final String VERSION = "v"+MAJOR_VERSION+"."+MINOR_VERSION+"."+BUILD_LABEL;
	
	public static boolean isVersionOld(String packVersion) {
		if( packVersion == null ) return false;	// can't check anything if they don't tell us
		String parts[] = packVersion.split("\\.");
		try {
			int mcuParts[] = { API_MAJOR, API_MINOR };
			for( int q = 0; q < mcuParts.length && q < parts.length; ++q ) {
				int packPart = Integer.valueOf(parts[q]);
				if( packPart > mcuParts[q] ) return true;
				if( packPart < mcuParts[q] ) return false; // Since we check major, then minor, then build, if the required value < current value, we can stop checking.
			}
			return false;
		} catch( NumberFormatException e ) {
			log("Got non-numerical pack format version '"+packVersion+"'");
		} catch( ArrayIndexOutOfBoundsException e ) {
			log("Got malformed pack format version '"+packVersion+"'");
		}
		return false;
	}
	
	public static boolean requestedFeatureLevel(String packVersion, String featureLevelVersion) {
		String packParts[] = packVersion.split("\\.");
		String featureParts[] = featureLevelVersion.split("\\.");
		try {
			for (int q = 0; q < featureParts.length; ++q ) {
				if (Integer.valueOf(packParts[q]) > Integer.valueOf(featureParts[q])) return true;
				if (Integer.valueOf(packParts[q]) < Integer.valueOf(featureParts[q])) return false;
			}
			return true;
		} catch( NumberFormatException e ) {
			log("Got non-numerical pack format version '"+packVersion+"'");
		} catch( ArrayIndexOutOfBoundsException e ) {
			log("Got malformed pack format version '"+packVersion+"'");
		}
		return false;
	}
	
	public static boolean isMasterBranch() {
		return BUILD_BRANCH.equals("master");
	}
	public static boolean isDevBranch() {
		return BUILD_BRANCH.equals("develop");
	}
	
	// for error logging support
	public static void setApp( MCUApp app ) {
		_app = app;
	}
	private static MCUApp _app;
	private static void log(String msg) {
		if( _app != null ) {
			_app.log(msg);
		} else {
			System.out.println(msg);
		}
	}
	public static void main(String[] srgs) {
	    System.out.println("Version=" + VERSION);
	}
}
