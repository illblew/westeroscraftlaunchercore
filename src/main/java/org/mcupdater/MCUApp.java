package org.mcupdater;

import java.io.File;
import java.util.Collection;
import java.util.logging.*;

import org.mcupdater.download_lib.DownloadQueue;
import org.mcupdater.download_lib.Downloadable;
import org.mcupdater.model.ServerList;
import org.mcupdater.mojang.MinecraftVersion;

public abstract class MCUApp {
	
	public static Logger baseLogger;

	public abstract void setStatus(String string);
	//public abstract void setProgressBar(int i);
	public abstract void addProgressBar(String title, String parent);
	public abstract void log(String msg);
	public abstract boolean requestLogin();
	public abstract void addServer(ServerList entry);
    public abstract DownloadQueue submitAssetsQueue(String queueName, String parent, MinecraftVersion version);
	public abstract DownloadQueue submitNewQueue(String queueName, String parent, Collection<Downloadable> files, File basePath, File cachePath);
}