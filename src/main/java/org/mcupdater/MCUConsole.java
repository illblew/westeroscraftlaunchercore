package org.mcupdater;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class MCUConsole extends Composite {

	private StyledText console;

	public static class LogLine {
        public String msg;
    }

	public MCUConsole(Composite parent) {
		super(parent, SWT.NONE);
		this.setLayout(new FillLayout());
		console = new StyledText(this, SWT.V_SCROLL | SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
		FontData[] fdConsole = console.getFont().getFontData();
		fdConsole[0].setHeight(10);
		console.setFont(new Font(Display.getCurrent(), fdConsole));
		console.addKeyListener(new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.stateMask == SWT.CONTROL && arg0.keyCode == 'a') {
					console.selectAll();
				}
			}

			@Override
			public void keyReleased(KeyEvent arg0) {}
			
		});
		console.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0) {
				console.setTopIndex(console.getLineCount()-1);
			}
		});
	}

	public void appendLine(final String text) {
		console.append(text + "\n");
	}

   public void appendLines(List<LogLine> lines) {
       StringBuilder sb = new StringBuilder();
       for (LogLine ll : lines) {
           sb.append(ll.msg).append('\n');
       }
       if (sb.length() > 0) {
           console.append(sb.toString());
       }
    }
}
